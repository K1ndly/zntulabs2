/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPlainTextEdit *plainTextEdit;
    QLabel *label;
    QPushButton *Sloc;
    QPushButton *EmtyLines;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_5;
    QPushButton *Comments;
    QLabel *label_6;
    QPushButton *VocOperandn2;
    QPushButton *OnlyCode;
    QLabel *label_4;
    QLabel *label_7;
    QPushButton *VocOperatnl;
    QPushButton *AllOperatN1;
    QLabel *label_8;
    QLabel *label_9;
    QPushButton *AllOperandN2;
    QPushButton *Cyclomatic;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QPushButton *Voc_clicked;
    QPushButton *N;
    QPushButton *V;
    QPushButton *CI;
    QLabel *label_14;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1181, 774);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        plainTextEdit = new QPlainTextEdit(centralWidget);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));
        plainTextEdit->setGeometry(QRect(480, 50, 481, 481));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(160, 70, 46, 13));
        Sloc = new QPushButton(centralWidget);
        Sloc->setObjectName(QStringLiteral("Sloc"));
        Sloc->setGeometry(QRect(230, 60, 131, 31));
        EmtyLines = new QPushButton(centralWidget);
        EmtyLines->setObjectName(QStringLiteral("EmtyLines"));
        EmtyLines->setGeometry(QRect(230, 110, 131, 31));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(160, 120, 46, 13));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(160, 160, 46, 13));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(160, 200, 46, 13));
        Comments = new QPushButton(centralWidget);
        Comments->setObjectName(QStringLiteral("Comments"));
        Comments->setGeometry(QRect(230, 190, 131, 31));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(160, 240, 46, 13));
        VocOperandn2 = new QPushButton(centralWidget);
        VocOperandn2->setObjectName(QStringLiteral("VocOperandn2"));
        VocOperandn2->setGeometry(QRect(230, 230, 131, 31));
        OnlyCode = new QPushButton(centralWidget);
        OnlyCode->setObjectName(QStringLiteral("OnlyCode"));
        OnlyCode->setGeometry(QRect(230, 280, 131, 31));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(160, 290, 46, 13));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(160, 330, 46, 13));
        VocOperatnl = new QPushButton(centralWidget);
        VocOperatnl->setObjectName(QStringLiteral("VocOperatnl"));
        VocOperatnl->setGeometry(QRect(230, 330, 131, 31));
        AllOperatN1 = new QPushButton(centralWidget);
        AllOperatN1->setObjectName(QStringLiteral("AllOperatN1"));
        AllOperatN1->setGeometry(QRect(230, 370, 131, 31));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(160, 370, 46, 13));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(160, 420, 46, 13));
        AllOperandN2 = new QPushButton(centralWidget);
        AllOperandN2->setObjectName(QStringLiteral("AllOperandN2"));
        AllOperandN2->setGeometry(QRect(230, 410, 131, 31));
        Cyclomatic = new QPushButton(centralWidget);
        Cyclomatic->setObjectName(QStringLiteral("Cyclomatic"));
        Cyclomatic->setGeometry(QRect(230, 450, 131, 31));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(160, 460, 46, 13));
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(160, 500, 46, 13));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(160, 540, 46, 13));
        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(160, 590, 46, 13));
        Voc_clicked = new QPushButton(centralWidget);
        Voc_clicked->setObjectName(QStringLiteral("Voc_clicked"));
        Voc_clicked->setGeometry(QRect(230, 490, 131, 31));
        N = new QPushButton(centralWidget);
        N->setObjectName(QStringLiteral("N"));
        N->setGeometry(QRect(230, 540, 131, 31));
        V = new QPushButton(centralWidget);
        V->setObjectName(QStringLiteral("V"));
        V->setGeometry(QRect(230, 590, 131, 31));
        CI = new QPushButton(centralWidget);
        CI->setObjectName(QStringLiteral("CI"));
        CI->setGeometry(QRect(230, 630, 131, 21));
        label_14 = new QLabel(centralWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(160, 630, 46, 13));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1181, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        Sloc->setText(QApplication::translate("MainWindow", "\320\232\320\276\320\273-\320\262\320\276  \321\201\321\202\321\200\320\276\320\272 \320\272\320\276\320\264\320\260", 0));
        EmtyLines->setText(QApplication::translate("MainWindow", "Emty", 0));
        label_2->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_3->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_5->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        Comments->setText(QApplication::translate("MainWindow", "Comments", 0));
        label_6->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        VocOperandn2->setText(QApplication::translate("MainWindow", "VocOperandn2", 0));
        OnlyCode->setText(QApplication::translate("MainWindow", "OnlyCode", 0));
        label_4->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_7->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        VocOperatnl->setText(QApplication::translate("MainWindow", "VocOperatnl", 0));
        AllOperatN1->setText(QApplication::translate("MainWindow", "AllOperatN1", 0));
        label_8->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_9->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        AllOperandN2->setText(QApplication::translate("MainWindow", "AllOperandN2", 0));
        Cyclomatic->setText(QApplication::translate("MainWindow", "Cyclomatic", 0));
        label_10->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_11->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_12->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_13->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        Voc_clicked->setText(QApplication::translate("MainWindow", "Voc_clicked", 0));
        N->setText(QApplication::translate("MainWindow", "N", 0));
        V->setText(QApplication::translate("MainWindow", "V", 0));
        CI->setText(QApplication::translate("MainWindow", "CI", 0));
        label_14->setText(QApplication::translate("MainWindow", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
